"""Setup script to create the sudoku3 package"""

# Import modules
import sys

from setuptools import setup, find_packages

from sudoku3_pygame.metadata import __package_name__, __author__, __author_email__, \
    __version__, __description__, __description_long__

if sys.version_info < (3, 6):
    raise RuntimeError('This package requires Python 3.6 or later')

setup(
    name=__package_name__,
    version=__version__,
    author=__author__,
    author_email=__author_email__,
    url='https://bitbucket.org/batterio/sudoku3_pygame',
    download_url=f'https://bitbucket.org/batterio/sudoku3_pygame/get/{__version__}.tar.gz',
    description=__description__,
    long_description=__description_long__,
    py_modules=['sudoku3_pygame'],
    packages=find_packages(exclude=['test*']),
    license='GPLv3',
    package_data={
        'sudoku3_pygame': [
            'sudoku3_pygame/fonts/*.ttf',
            'sudoku3_pygame/images/*.jpg',
            'sudoku3_pygame/images/*.png',
            'sudoku3_pygame/sounds/*.mp3',
        ]
    },
    include_package_data=True,
    install_requires=[
        'click',
        'daiquiri',
        'sudoku3',
        'pygame'
    ],
    entry_points={
        'console_scripts': [
            'sudoku3_pygame = sudoku3_pygame.main:main',
        ]
    },
    classifiers=[
        "Development Status :: 5 - Production/Stable",
        "Environment :: Console",
        "Intended Audience :: End Users/Desktop",
        "License :: OSI Approved :: MIT License",
        "Natural Language :: English",
        "Operating System :: OS Independent",
        "Programming Language :: Python",
        "Programming Language :: Python :: 3.6",
        "Programming Language :: Python :: 3.7",
        "Topic :: Games/Entertainment",
        "Topic :: Games/Entertainment :: Puzzle Games",
    ],
)
