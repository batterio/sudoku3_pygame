# -*- mode: python -*-

block_cipher = None


a = Analysis(['sudoku3_pygame/main.py'],
             pathex=['/home/loris/personal/projects/sudoku3_pygame'],
             binaries=[],
             datas=[
                ('sudoku3_pygame/fonts/Neon.ttf', 'sudoku3_pygame/fonts'),
                ('sudoku3_pygame/fonts/handwriting-draft_free-version.ttf', 'sudoku3_pygame/fonts'),
                ('sudoku3_pygame/fonts/Superscratchy.ttf', 'sudoku3_pygame/fonts'),
                ('sudoku3_pygame/images/Texturas Vector.jpg', 'sudoku3_pygame/images'),
                ('sudoku3_pygame/images/sound_on.png', 'sudoku3_pygame/images'),
                ('sudoku3_pygame/images/sound_off.png', 'sudoku3_pygame/images'),
                ('sudoku3_pygame/images/DD-Grunge-Background-88734-Preview.jpg', 'sudoku3_pygame/images'),
                ('sudoku3_pygame/images/DD-Grunge-Background-43120-Preview.jpg', 'sudoku3_pygame/images'),
                ('sudoku3_pygame/sounds/Lost-Jungle_Looping.mp3', 'sudoku3_pygame/sounds'),
             ],
             hiddenimports=[],
             hookspath=[],
             runtime_hooks=[],
             excludes=[],
             win_no_prefer_redirects=False,
             win_private_assemblies=False,
             cipher=block_cipher,
             noarchive=False)
pyz = PYZ(a.pure, a.zipped_data,
             cipher=block_cipher)
exe = EXE(pyz,
          a.scripts,
          a.binaries,
          a.zipfiles,
          a.datas,
          [],
          name='sudoku_pygame',
          debug=False,
          bootloader_ignore_signals=False,
          strip=False,
          upx=True,
          runtime_tmpdir=None,
          console=True )
