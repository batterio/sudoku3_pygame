# Import modules
import os
from distutils.version import LooseVersion
import logging
from pip._internal import main as pipmain
from pip._internal.commands.list import ListCommand
from pip._internal.utils.misc import get_installed_distributions
from urllib.request import urlopen
from urllib.error import HTTPError, URLError

import daiquiri
import pygame
from sudoku3.metadata import __version__ as sudoku3_version

from sudoku3_pygame.metadata import __version__ as sudoku3_pygame_version
from sudoku3_pygame.settings import BITBUCKET_API, REPOSITORIES, font_neon as font


daiquiri.setup(level=logging.INFO)
logger = daiquiri.getLogger('updates')


class Updates:

    def __init__(self):
        """initialize the class"""

        self.updates = self.check_updates()

    @staticmethod
    def _install_update(package):
        """Install a package through pip
        Examples:

        pip install --upgrade --src="$HOME/.src" -e git+<URL>@<REV>#egg=PACKAGE_NAME
        pip install --upgrade --src="$HOME/.src" -e git+https://github.com/user/repo.git@<VERSION>#egg=PACKAGE_NAME

        :param package: package to install
        :type package: str
        :return: None
        :rtype: None
        """

        pipmain(['install', '--upgrade', '-e', f'git+{package}'])

    @staticmethod
    def _compare_versions(system_version, remote_url, name):
        """Compare the installed version of a software with the
        one present on the repository

        :param system_version: version of the installed software
        :type system_version: str
        :param remote_url: url of the repository of the software
        :type remote_url: str
        :param name: name of the software
        :type name: str
        :return: True if the software has an available update, False otherwise
        :rtype: bool
        """

        # Retrieve the bitbucket pages
        exec_namespace = {}
        try:
            response = urlopen(remote_url)
            res = response.read().decode()
            exec(res, exec_namespace)
            remote_version = exec_namespace['__version__']
        except HTTPError:
            logger.error(f'Could not connect to the {name} repository (HTTP Error)')
            return False
        except URLError:
            logger.error(f'Could not connect to the {name} repository (URL Error)')
            return False
        except Exception:
            logger.error(f'Could not connect to the {name} repository (Unknown Error)')
            return False

        if LooseVersion(system_version) < LooseVersion(remote_version):
            logger.info(f'Found a new version of {name}')
            return True
        else:
            logger.info(f'{name} is up to date')
            return False

    @staticmethod
    def check_updates():
        """Check if there are newer version available

        :return: list of packages to update
        :rtype: list
        """

        updates = []
        sudoku3_pygame_remote_version = os.path.join(BITBUCKET_API.format('sudoku3_pygame', 'sudoku3_pygame'))
        sudoku3_remote_version = os.path.join(BITBUCKET_API.format('sudoku3', 'sudoku3'))

        for system_version, remove_url, name in zip(
            (sudoku3_pygame_version, sudoku3_version),
            (sudoku3_pygame_remote_version, sudoku3_remote_version),
            ('sudoku3_pygame', 'sudoku3')
        ):

            if Updates._compare_versions(system_version, remove_url, name):
                updates.append(REPOSITORIES[name])

        return updates

    @staticmethod
    def check_updates_pip():
        """Check whether there are updated version of
        the installed libraries on pypi

        :return: list of outdated packages
        :rtype: list
        """

        list_command = ListCommand()
        options, args = list_command.parse_args([])
        packages = get_installed_distributions()
        outdated = list_command.get_outdated(packages, options)
        return outdated


class UpdateGUI:

    def __init__(self, packages):
        """Initialize the class"""

        self.packages = packages
        pygame.init()
        self.font = font

        self.surface = pygame.display.set_mode((400, 300))
        pygame.display.set_caption('Sudoku3 update')
        self.mouse_position = pygame.mouse.get_pos()
        self.button_yes = pygame.draw.rect(self.surface, (123, 123, 123), (50, 200, 100, 50))
        self.button_no = pygame.draw.rect(self.surface, (123, 123, 123), (250, 200, 100, 50))
        self.run()

    def _update(self):
        """Install the updates using the _install_update method of the Updates class

        :return: None
        :rtype: None
        """

        for package in self.packages:
            Updates._install_update(package)

    def _draw_buttons(self):
        """Draw two buttons on the surface

        :return: None
        :rtype: None
        """

        color = (123, 123, 123) if self.button_yes.collidepoint(self.mouse_position) else (150, 150, 150)
        color_text = (165, 0, 0) if self.button_yes.collidepoint(self.mouse_position) else (43, 100, 145)

        self.button_yes = pygame.draw.rect(self.surface, color, (50, 200, 100, 50))
        text = self.font.render('Update', False, color_text)
        text_rect = text.get_rect(center=(100, 225))
        self.surface.blit(text, text_rect)

        color = (123, 123, 123) if self.button_no.collidepoint(self.mouse_position) else (150, 150, 150)
        color_text = (165, 0, 0) if self.button_no.collidepoint(self.mouse_position) else (43, 100, 145)

        self.button_no = pygame.draw.rect(self.surface, color, (250, 200, 100, 50))
        text = self.font.render('Cancel', False, color_text)
        text_rect = text.get_rect(center=(300, 225))
        self.surface.blit(text, text_rect)

    def _write_message(self):
        """Write a message on the surface

        :return: None
        :rtype: None
        """

        text_1 = self.font.render('There is an update for Sudoku3.', False, (0, 0, 0))
        text_2 = self.font.render('What do you want to do?', False, (0, 0, 0))
        text_rect_1 = text_1.get_rect(center=(200, 80))
        text_rect_2 = text_2.get_rect(center=(200, 130))
        self.surface.blit(text_1, text_rect_1)
        self.surface.blit(text_2, text_rect_2)

    def run(self):
        """Draw the window

        :return: None
        :rtype: None
        """

        quit = False
        update = False

        while not quit:
            self.surface.fill((100, 100, 100))
            self._write_message()
            self._draw_buttons()

            self.mouse_position = pygame.mouse.get_pos()

            # Check whether the mouse is pressed down
            if pygame.mouse.get_pressed()[0] == 1:
                if self.button_yes.collidepoint(self.mouse_position):
                    update = True
                    quit = True
                elif self.button_no.collidepoint(self.mouse_position):
                    update = False
                    quit = True

                pygame.time.delay(200)

            for event in pygame.event.get():
                if event.type == pygame.locals.QUIT:
                    update = False
                    quit = True

            pygame.display.update()

        if update:
           self._update()

        # Reset the size of the surface
        self.surface = pygame.display.set_mode((480, 640))


# TODO: Check the automatic update process in self._update()
