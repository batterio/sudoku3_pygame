VERSION = (0, 3, 2)
__package_name__ = 'sudoku3_pygame'
__author__ = 'Loris Mularoni'
__author_email__ = 'loris.mularoni@gmail.com'
__version__ = '.'.join([str(i) for i in VERSION])
__description__ = 'PyGame GUI for Sudoku3'
__description_long__ = 'PyGame GUI for Sudoku3. Sudoku3 is a python3 implementation of both a sudoku solver and a sudoku generator'
