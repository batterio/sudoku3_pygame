# Import modules
import sys

import pygame
import pygame.event as GAME_EVENTS
import pygame.locals as GAME_GLOBALS
import pygame.time as GAME_TIME
from sudoku3.generator import Generator

from sudoku3_pygame.board import Board
from sudoku3_pygame.settings import COLOR_KNOWN_NUMBER, COLOR_CORRECT_NUMBER, \
    COLOR_WRONG_NUMBER, NUMBER_KEYS, ARROW_KEYS, SIZE, WINDOW_WIDTH, WINDOW_HEIGHT
from sudoku3_pygame.settings import font_handwriting as font, font_superscratchy as font_clock
from sudoku3_pygame.settings import init_background, running_background
from sudoku3_pygame.settings import music_path
from sudoku3_pygame.updates import Updates, UpdateGUI


# Global variables
insert_panel_position = None
mouse_position = (0, 0)
numbers = None
game_over = False
game_solved = False


pygame.init()


surface = pygame.display.set_mode((WINDOW_WIDTH, WINDOW_HEIGHT))
pygame.display.set_caption('Sudoku3')


# Load sounds
pygame.mixer.init()
pygame.mixer_music.load(music_path)


class Timer:

    def __init__(self):
        self.clock = GAME_TIME.Clock()
        self._elapsed = 0
        self._elapsed_time = ''
        self.is_paused = False

    @property
    def elapsed_time(self):
        return self._elapsed_time

    def update(self):
        _ = self.clock.tick()
        if not self.is_paused:
            self._elapsed += self.clock.get_time()

        seconds = self._elapsed // 1000
        minutes, seconds = divmod(seconds, 60)
        hours, minutes = divmod(minutes, 60)
        self._elapsed_time = f'{hours:02}:{minutes:02}:{seconds:02}' if hours > 0 else f'{minutes:02}:{seconds:02}'

    def pause(self):
        self.is_paused = True

    def restart(self):
        self.is_paused = False


class Game:

    def __init__(self, window_width, window_height, size):
        self.window_width = window_width
        self.window_height = window_height
        self.size = size
        self.axis = self.size ** 2
        self.sudoku = None
        self.solution = None
        self._numbers = None
        self.clock = None
        self.help = False
        self.is_running = True

    def start_timer(self):
        self.is_running = True
        self.clock = Timer()

    def stop_timer(self):
        self.is_running = False

    def pause_timer(self):
        self.clock.pause()

    def restart_timer(self):
        self.clock.restart()

    @property
    def numbers(self):
        return self._numbers

    def init(self):
        text = font.render('Generating Sudoku...', False, (0, 0, 0))
        text_rect = text.get_rect(center=(self.window_width / 2, self.window_height / 2))
        surface.blit(init_background, [0, 0])
        surface.blit(text, text_rect)
        pygame.display.update()

    def over(self):
        pygame.draw.rect(surface, (100, 100, 100), (self.window_width / 2 - 175, self.window_height / 2 - 50, 350, 100))
        text = font.render('Sudoku Solved!', False, (0, 0, 0))
        text_rect = text.get_rect(center=(self.window_width / 2, self.window_height / 2))
        # surface.blit(background_image, [0, 0])
        surface.blit(text, text_rect)
        # pygame.display.update()

    @staticmethod
    def quit():
        pygame.quit()
        sys.exit()

    def generate_sudoku(self, optimize=True, unique=True):
        # Create an instance of the Generator class
        self.sudoku = Generator(size=self.size)

        # Generate a new sudoku
        self.sudoku.generate()

        if optimize:
            # Optimize the generated sudoku by removing extra numbers
            self.sudoku.optimize()

        if not unique:
            # Force the generated sudoku to have more than one possible solution
            self.sudoku.unique_to_multiple_solutions()

        # Retrieve the results
        result = self.sudoku.result
        solution = self.sudoku.solution

        # result.print_sudoku()
        # print(f'Sudoku: {result.matrix_to_string(include_null=True)}')
        # print(f'Solution: {solution}')
        # print(f'Hints: {self.sudoku.get_number_hints()}')
        # print(f'Score: {self.sudoku.get_score()}')
        # print(self.sudoku.result.matrix_to_string(include_null=True))

        self.solution = self.sudoku.solution.matrix_to_string(include_null=False)
        self.reset()

    def timer(self):
        if self.clock is not None:
            if self.is_running:
                self.clock.update()
            text = font_clock.render(f'{self.clock.elapsed_time}', False, (0, 0, 0))
            text_rect = text.get_rect(center=(self.window_width / 2, 20))
            surface.blit(text, text_rect)
            pygame.display.update()

    def reset(self):
        self._numbers = {}
        for i, n in enumerate(self.sudoku.result.matrix_to_string(include_null=True)):
            x = i % self.axis
            y = i // self.axis
            self._numbers[(x, y)] = {
                'x': x,
                'y': y,
                'value': n if n != '.' else '',
                'solution': self.solution[i],
                'color': COLOR_KNOWN_NUMBER,
                'editable': True if n == '.' else False,
            }
        self.start_timer()

    def solve(self):
        self.help = True
        self._numbers = {}
        for i, n in enumerate(self.sudoku.result.matrix_to_string(include_null=True)):
            x = i % self.axis
            y = i // self.axis
            self._numbers[(x, y)] = {
                'x': x,
                'y': y,
                'value': self.solution[i],
                'solution': self.solution[i],
                'color': COLOR_KNOWN_NUMBER if n != '.' else COLOR_CORRECT_NUMBER,
                'editable': True if n == '.' else False,
            }

    def is_complete(self):
        for i, n in enumerate(self.solution):
            x = i % self.axis
            y = i // self.axis
            if self._numbers[(x, y)]['value'] != n:
                return False
        return True


game = Game(WINDOW_WIDTH, WINDOW_HEIGHT, size=SIZE)
board = Board(surface=surface, size=SIZE)


def run():
    global numbers
    global game_over
    global game_solved
    global status

    while True:
        surface.blit(running_background, [0, 0])

        if game.sudoku is None:
            game.init()
            game.generate_sudoku()
            pygame.mixer.music.play(-1)

        board.draw()
        board.fill_numbers(game.numbers, delay=False)
        if game_over:
            board.game_over()
        game.timer()

        # if game_over:
        #     game.over()

        # Update mouse position
        board.get_mouse_position()

        # Check whether the mouse is pressed down
        if pygame.mouse.get_pressed()[0] == 1:  # Left, center, Right
            xy = board.find_xy()
            if xy['type'] == 'cell':
                board.selected = {
                    'x': xy['x'] // 50,
                    'y': xy['y'] // 50 - 1
                }
            elif xy['type'] == 'volume':
                board.sound = not board.sound
                if board.sound:
                    pygame.mixer.music.unpause()
                else:
                    pygame.mixer_music.pause()
            elif xy['type'] == 'New':
                game.sudoku = None
                game_over = False
                pygame.mixer.music.stop()
            elif xy['type'] == 'Reset':
                game.reset()
                game_over = False
            elif xy['type'] == 'Solve':
                game.solve()
                game_solved = True
            elif xy['type'] == 'Pause':
                board.pause = True
                game.pause_timer()
            elif xy['type'] == 'Continue':
                board.pause = False
                game.restart_timer()

            GAME_TIME.delay(200)

        for event in GAME_EVENTS.get():

            if event.type == pygame.KEYDOWN:  # or pygame.KEYUP

                # Insertion of a number
                if event.key in NUMBER_KEYS.keys():
                    # Get the coordinates of the selected cell
                    selected = board.selected
                    solution = game.numbers[(selected['x'], selected['y'])]['solution']
                    editable = game.numbers[(selected['x'], selected['y'])]['editable']
                    value = NUMBER_KEYS[event.key]

                    if editable:
                        game.numbers[(selected['x'], selected['y'])] = {
                            'x': selected['x'],
                            'y': selected['y'],
                            'value': value,
                            'solution': solution,
                            'color': COLOR_CORRECT_NUMBER if solution == value else COLOR_WRONG_NUMBER,
                            'editable': editable,
                        }

                # Movements
                elif event.key in ARROW_KEYS.keys():
                    board.move(ARROW_KEYS[event.key])

            elif event.type == GAME_GLOBALS.QUIT:
                game.quit()

        # Check if the game is over
        if game_over is False and game.is_complete():
            game.stop_timer()
            if not game.help:
                game_over = True

        pygame.display.update()


def set_seed(seed: int) -> None:
    """Set the seed for reproducibility

    :param seed: seed to set the random module
    :type seed: int
    :return: None
    :rtype: None
    """

    import random
    random.seed(seed)


def main(seed: int = -1) -> None:
    """Python3_pygame: A GUI for sudoku3 (sudoku generator and sudoku solver)"""

    # Check for updates
    updates = Updates()
    if updates.updates:
        # Show message
        UpdateGUI(updates.updates)

    if seed >= 0:
        set_seed(seed)
    run()


if __name__ == '__main__':
    main()
