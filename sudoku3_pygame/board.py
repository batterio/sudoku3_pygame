# Import modules
import math

import pygame

from sudoku3_pygame.settings import BUTTON_NEW, BUTTON_PAUSE, BUTTON_SOLVE, \
    BUTTON_RESET, BUTTON_CONTINUE, COLOR_KNOWN_NUMBER, COLOR_WRONG_NUMBER, \
    WINDOW_WIDTH, WINDOW_HEIGHT
from sudoku3_pygame.settings import font_neon as font, font_handwriting as pause_font
from sudoku3_pygame.settings import pause_background, volume_on, volume_off


# Global variables
volume_on = pygame.transform.scale(volume_on, (30, 30))
volume_off = pygame.transform.scale(volume_off, (30, 30))


class Board:
    """Class with the specifics of the sudoku boards"""

    def __init__(self, surface, size):
        """Initialize the Board class

        :param surface: windows of the application
        :type surface: pygame surface object
        :param size: size of the sudoku
        :type size: int
        """

        self.surface = surface
        self.size = size
        self.insert_mode = False
        self.mouse_position = None
        self.get_mouse_position()
        self.selected = {'x': 4, 'y': 4}
        self.pause = False
        self.sound = True

    def get_mouse_position(self):
        """Get the position of the mouse

        :return: coordinates (x, y) of the mouse pointer
        :rtype: tuple
        """

        self.mouse_position = pygame.mouse.get_pos()

    def draw(self):
        """Draw the layout of the game

        :return:
        """

        if self.pause:
            self._draw_pause()
        else:
            self._draw_grid()
        self._draw_buttons()
        self._draw_volume()

    def _draw_pause(self):
        """Draw the pause layout

        :return: None
        :rtype: None
        """

        text = pause_font.render('Game paused', False, (0, 0, 0))
        text_rect = text.get_rect(center=(WINDOW_WIDTH / 2, WINDOW_HEIGHT * 0.4))
        self.surface.blit(pause_background, [0, 0])
        self.surface.blit(text, text_rect)

    def _draw_grid(self):
        """Draw the sudoku grid

        :return: None
        :rtype: None
        """

        for (x, y) in zip((15, 315, 165, 15, 315), (50, 50, 200, 350, 350)):
            pygame.draw.rect(
                self.surface,
                (150, 150, 150),
                (x, y, 150, 150)
            )

        pygame.draw.rect(self.surface, (0, 0, 0), (15, 50, 450, 450), 5)

        for pos in range(65, 450, 50):
            thick = 3 if pos in (165, 315) else 1
            pygame.draw.line(self.surface, (0, 0, 0), (pos, 50), (pos, 500), thick)

        for pos in range(100, 500, 50):
            thick = 3 if pos in (200, 350) else 1
            pygame.draw.line(self.surface, (0, 0, 0), (15, pos), (465, pos), thick)

        # if self.insert_mode:
        self.change_borders_cell()

    def _draw_buttons(self):
        """Draw a series of button below the sudoku grid

        :return: None
        :rtype: None
        """

        coords = self.find_xy()

        if self.pause:
            buttons = (BUTTON_CONTINUE, )
        else:
            buttons = (BUTTON_NEW, BUTTON_RESET, BUTTON_SOLVE, BUTTON_PAUSE)

        for button in buttons:
            color = button['selected'] if coords['type'] == button['label'] else button['normal']
            color_text = COLOR_WRONG_NUMBER if coords['type'] == button['label'] else COLOR_KNOWN_NUMBER
            pygame.draw.rect(self.surface, color, button['coords'])
            text = font.render(button['label'], False, color_text)
            text_rect = text.get_rect(center=button['middle'])
            self.surface.blit(text, text_rect)

    def _draw_volume(self):
        """Draws the sound icons

        :return: None
        :rtype: None
        """

        if self.sound:
            self.surface.blit(volume_on, [435, 10])
        else:
            self.surface.blit(volume_off, [435, 10])

    def game_over(self):
        """Print a message notifying that the sudoku has been solved

        :return: None
        :rtype: None
        """

        if not self.pause:
            pygame.draw.rect(self.surface, (100, 100, 100), (WINDOW_WIDTH / 2 - 175, WINDOW_HEIGHT / 2 - 50, 350, 100))
            text = pause_font.render('Sudoku Solved!', False, (0, 0, 0))
            text_rect = text.get_rect(center=(WINDOW_WIDTH / 2, WINDOW_HEIGHT / 2))
            self.surface.blit(text, text_rect)

    def move(self, direction):
        """Move the selected cell

        :param direction: direction of the movement
        :type direction: str
        :return: None
        :rtype: None
        """

        if direction == 'up':
            self.selected['y'] -= 1 if self.selected['y'] > 0 else 0
        elif direction == 'down':
            self.selected['y'] += 1 if self.selected['y'] < (self.size ** 2 - 1) else 0

        elif direction == 'right':
            self.selected['x'] += 1 if self.selected['x'] < (self.size ** 2 - 1) else 0
        elif direction == 'left':
            self.selected['x'] -= 1 if self.selected['x'] > 0 else 0

    def fill_cell(self, x, y, value, color):
        """Write a value in a cell

        :param x: x coordinate
        :type x: int
        :param y: y coordinate
        :type y: int
        :param value: number to write
        :type value: str
        :param color: color of the value string
        :type color: tuple
        :return: None
        :rtype: None
        """

        text = font.render(value, False, color)
        text_rect = text.get_rect(center=(x * 50 + 15 + 25, y * 50 + 50 + 25))
        self.surface.blit(text, text_rect)

    def fill_numbers(self, numbers, delay=False):
        """Fill the sudoku grid with numbers

        :param numbers: values of each position of the sudoku
        :type numbers: dict
        :param delay: waits a little bit after writing a number
        :type delay: bool
        :return: None
        :rtype: None
        """
        if not self.pause:
            if delay:
                for key, value in numbers.items():
                    if value['color'] == COLOR_KNOWN_NUMBER:
                        self.fill_cell(
                            x=value['x'],
                            y=value['y'],
                            value=value['value'],
                            color=value['color']
                        )
                pygame.display.update()
                for key, value in numbers.items():
                    if value['color'] != COLOR_KNOWN_NUMBER:
                        self.fill_cell(
                            x=value['x'],
                            y=value['y'],
                            value=value['value'],
                            color=value['color']
                        )
                        pygame.time.delay(100)
                        pygame.display.update()
            else:
                for key, value in numbers.items():
                    self.fill_cell(
                        x=value['x'],
                        y=value['y'],
                        value=value['value'],
                        color=value['color']
                    )

    def find_xy(self):
        """Return the coordinates in the board of the
        cell selected by the mouse

        :return: coordinates of the cell selected by the mouse
        :rtype: dict
        """

        x, y = self.mouse_position
        if not self.pause and 15 <= x <= 465 and 50 <= y <= 500:
            x = math.floor((x - 15) / 50) * 50 + 15
            y = math.floor((y - 50) / 50) * 50 + 50
            return {'type': 'cell', 'x': x, 'y': y}
        elif 440 <= x <= 500 and 10 <= y <= 60:
            return {'type': 'volume', 'x': x, 'y': y}
        if self.pause:
            buttons = (BUTTON_CONTINUE, )
        else:
            buttons = (BUTTON_NEW, BUTTON_RESET, BUTTON_SOLVE, BUTTON_PAUSE)
        for button in buttons:
            if (button['coords'][0] <= x <= button['coords'][0] + button['coords'][2]) and \
                    (button['coords'][1] <= y <= button['coords'][1] + button['coords'][3]):
                return {'type': button['label'], 'x': x, 'y': y}

        return {'type': None}

    def convert_coordinates(self):
        """Convert the coordinates of the board in
        coordinates of the sudoku grid

        :return: coordinates of a cell in the sudoku grid
        :rtype: dict
        """

        x = self.selected['x'] * 50 + 15
        y = (self.selected['y'] + 1) * 50
        return {'x': x, 'y': y}

    def change_borders_cell(self):
        """Change the color of the border of a cell

        :return: None
        :rtype: None
        """

        selected = self.convert_coordinates()
        pygame.draw.rect(
            self.surface,
            (109, 207, 246),
            (selected['x'], selected['y'], 50, 50),
            3
        )
