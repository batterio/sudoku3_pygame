# Import modules
import os
import pygame


# Global variables
SIZE = 3
WINDOW_WIDTH = 480
WINDOW_HEIGHT = 640

COLOR_KNOWN_NUMBER = (43, 100, 145)
COLOR_CORRECT_NUMBER = (38, 38, 38)
COLOR_WRONG_NUMBER = (165, 0, 0)

BUTTON_NEW = {
    'label': 'New',
    'coords': (15, 550, 96, 40),
    'middle': (15 + 48, 570),
    'normal': (123, 123, 123),  # 227, 74, 51),
    'selected': (150, 150, 150),  # 253, 187, 132)
}

BUTTON_RESET = {
    'label': 'Reset',
    'coords': (133, 550, 96, 40),
    'middle': (133 + 48, 570),
    'normal': (123, 123, 123),  # (43, 140, 190),
    'selected': (150, 150, 150),  # (166, 189, 219)
}

BUTTON_SOLVE = {
    'label': 'Solve',
    'coords': (251, 550, 96, 40),
    'middle': (251 + 48, 570),
    'normal': (123, 123, 123),  # (43, 140, 190),
    'selected': (150, 150, 150),  # (166, 189, 219)
}

BUTTON_PAUSE = {
    'label': 'Pause',
    'coords': (369, 550, 96, 40),
    'middle': (369 + 48, 570),
    'normal': (123, 123, 123),  # (49, 163, 84),
    'selected': (150, 150, 150),  # (161, 217, 155)
}

BUTTON_CONTINUE = {
    'label': 'Continue',
    'coords': (WINDOW_WIDTH / 2 - 60, WINDOW_HEIGHT * 0.6 - 20, 120, 40),
    'middle': (WINDOW_WIDTH / 2, WINDOW_HEIGHT * 0.6),
    'normal': (123, 123, 123),  # (49, 163, 84),
    'selected': (150, 150, 150),  # (161, 217, 155)
}

NUMBER_KEYS = {
    pygame.K_1: '1',
    pygame.K_2: '2',
    pygame.K_3: '3',
    pygame.K_4: '4',
    pygame.K_5: '5',
    pygame.K_6: '6',
    pygame.K_7: '7',
    pygame.K_8: '8',
    pygame.K_9: '9',
    pygame.K_0: '',
    pygame.K_DELETE: '',
    pygame.K_BACKSPACE: '',
}

ARROW_KEYS = {
    pygame.K_DOWN: 'down',
    pygame.K_UP: 'up',
    pygame.K_RIGHT: 'right',
    pygame.K_LEFT: 'left'
}

BITBUCKET_API = 'https://api.bitbucket.org/2.0/repositories/batterio/{}/src/master/{}/metadata.py'

REPOSITORIES = {
    'sudoku3': 'https://batterio@bitbucket.org/batterio/sudoku3.git',
    'sudoku3_pygame': 'https://batterio@bitbucket.org/batterio/sudoku3_pygame.git',
}


# Fonts
pygame.font.init()
path = os.path.dirname(__file__)
font_handwriting = pygame.font.Font(os.path.join(path, 'fonts', 'handwriting-draft_free-version.ttf'), 38)  # bonzai
font_superscratchy = pygame.font.Font(os.path.join(path, 'fonts', 'Superscratchy.ttf'), 32)  # bonzai
font_neon = pygame.font.Font(os.path.join(path, 'fonts', 'Neon.ttf'), 28)


# Images
init_background = pygame.image.load(os.path.join(path, 'images', 'DD-Grunge-Background-88734-Preview.jpg'))
running_background = pygame.image.load(os.path.join(path, 'images', 'DD-Grunge-Background-43120-Preview.jpg'))
pause_background = pygame.image.load(os.path.join(path, 'images', 'Texturas Vector.jpg'))
volume_on = pygame.image.load(os.path.join(path, 'images', 'sound_on.png'))
volume_off = pygame.image.load(os.path.join(path, 'images', 'sound_off.png'))


# Sounds
music_path = os.path.join(path, 'sounds', 'Lost-Jungle_Looping.mp3')