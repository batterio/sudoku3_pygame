PySudoku3 PyGame
================

This is a GUI for `sudoku3`, a python3 implementation of both a sudoku solver and a sudoku generator

Install the program with:
    pip install git+https://batterio@bitbucket.org/batterio/sudoku3_pygame.git

To run the program just type:
    sudoku3_pygame

A binary can be created with the library `PyInstaller`:
    pyinstaller --onefile main.spec

The binary file will be created inside the directory dist
with the name `sudoku3_pygame`

To Do list:
    - Docstring (0.4)
    - Add temporary numbers
    - Animate the solve function (counter, up to)
    - Check if there are inconsistencies in row, column, or block
    - Redo the board as an image
    - Redo buttons
    - Check for updates (installation)
